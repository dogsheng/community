# openEuler Java Special Interest Group (SIG)
English | [简体中文](./sig-Java_cn.md)

The openEuler Java SIG aims at providing Java package solution and guideline to openEuler community, in order to reduce the threshold for using openEuler and promote openEuler to new users, Java applications and Java components.

## SIG Mission and Scope

### Mission
- 

### Scope
- 

![missions](missions.png)


### Repositories and description managed by this SIG

- Repository of scripts, macros and docs for Java package: https://gitee.com/openeuler/Java-Packages

## Basic Information

### Project Introduction
    https://gitee.com/openeuler/community/tree/master/sig/sig-Java/

### Maintainers
- luo-haibo
- sinever
- it_bricklayer
- rita_dong

### Committers
- 

### Mailing list
- java-sig@openeuler.org

### Slack Workspace
- openeulerworkspace.slack.com
- Slack invitation link: https://join.slack.com/t/openeulerworkspace/shared_invite/zt-gwgkej74-O6O743~3LRM6LqPbnOPeww

### Meeting
- Time: Every Friday, 6:00 - 6:30 pm
- Zoom MeetID: 754 813 6418

### IRC Channel
- 

### External Contact
- 
