# openEuler CICD SIG

- 开发和维护 crystal-ci
- 支持在openEuler各项目应用

# 组织会议

- 公开的会议时间：待定

# 成员

- wu_fengguang
- jimmy_hero
- walkingwalk

### Maintainer列表

- 吴峰光[@wu_fengguang](https://gitee.com/wu_fengguang)
- 杜开田[@jimmy_hero](https://gitee.com/jimmy_hero)
- 伍伯东[@walkingwalk](https://gitee.com/walkingwalk)

### Committer列表

- 吴峰光[@wu_fengguang](https://gitee.com/wu_fengguang)
- 杜开田[@jimmy_hero](https://gitee.com/jimmy_hero)
- 伍伯东[@walkingwalk](https://gitee.com/walkingwalk)

# 联系方式

- [邮件列表](crystal-ci@openeuler.org)


# 项目清单

项目名称：crystal-ci

repository地址：

- https://gitee.com/openeuler/crystal-ci

