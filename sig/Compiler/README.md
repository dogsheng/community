## Compiler SIG
 - devoted to traditional compilers like gcc/llvm/OpenJDK/...
 - motivated to program optimization

## Weekly Meeting

Tuesday 10 am every week.

## Members

### Maintainers
 - 郭 歌[@jdkboy](https://gitee.com/jdkboy)
 - 章 海剑[@Haijian.Zhang](https://gitee.com/haijianzhang)

### Committers
 - 贺 东博[@Noah](https://gitee.com/jvmboy)
 - 谢 志恒[@eastb233](https://gitee.com/eastb233)

## Contact

 - maillist: dev@openeuler.org

## Repositories
 - [gcc](https://gitee.com/src-openeuler/gcc)
 - [llvm](https://gitee.com/src-openeuler/llvm)
 - [clang](https://gitee.com/src-openeuler/clang)
 - [compiler-rt](https://gitee.com/src-openeuler/compiler-rt)
 - [openjdk-1.8.0](https://gitee.com/src-openeuler/openjdk-1.8.0)
 - [openjdk-11](https:/gitee.com/src-openeuler/openjdk-11)
 - [openjdk-latest](https:/gitee.com/src-openeuler/openjdk-latest)
 - [openjfx8](https://gitee.com/src-openeuler/openjfx8)
 - [openjfx11](https://gitee.com/src-openeuler/openjfx11)
 - [icedtea-web](https://gitee.com/src-openeuler/icedtea-web)
 - [rust](https://gitee.com/src-openeuler/rust)
 - [Boole-JDK-8](https://gitee.com/openeuler/boolejdk-8)
 - [Boole-JDK-11](https://gitee.com/openeuler/boolejdk-11)
